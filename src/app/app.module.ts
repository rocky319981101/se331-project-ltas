import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { IndexComponent } from './index/index.component';
import {MaterialModule} from './material-module';
import {StudentLoginComponent} from './student-login/student-login.component';

import {ReactiveFormsModule} from '@angular/forms';
import {StudentRegComponent} from './student-reg/student-reg.component';
import { StudentComponent } from './student/student.component';
import {StudentActivityAllComponent} from './student-activity-all/student-activity-all.component';
import {StudentActivityEnrolledComponent} from './student-activity-enrolled/student-activity-enrolled.component';
import {StudentActivityConfirmedComponent} from './student-activity-confirmed/student-activity-confirmed.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    StudentLoginComponent,
    StudentRegComponent,
    StudentComponent,
    StudentActivityAllComponent,
    StudentActivityEnrolledComponent,
    StudentActivityConfirmedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
