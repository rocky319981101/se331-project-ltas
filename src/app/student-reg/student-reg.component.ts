import {Component, OnInit} from '@angular/core';
import {StudentService} from '../service/student.service';
import {FormBuilder, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-student-reg',
  templateUrl: './student-reg.component.html',
  styleUrls: ['./student-reg.component.scss']
})
export class StudentRegComponent implements OnInit {
  constructor(
    private studentService: StudentService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private router: Router
  ) {
  }

  regForm = this.fb.group({
    id: this.fb.control('', Validators.required),
    name: this.fb.control('', Validators.required),
    surname: this.fb.control('', Validators.required),
    birth: this.fb.control('', Validators.required),
    email: this.fb.control('', [Validators.required, Validators.email]),
    password: this.fb.control('', Validators.required),
    password2: this.fb.control('', Validators.required),
  });

  ngOnInit() {
  }

  onSubmit({name, surname, birth, id, email, password, password2}: {
    name: string;
    surname: string;
    birth: string;
    id: string;
    email: string;
    password: string;
    password2: string;
  }) {
    if (password !== password2) {
      alert('Two passwords are inconsistent.');
      return;
    }
    const ret = this.studentService.reg(name, surname, birth, id, email, password);
    if (ret.state === 'ok') {
      this.router.navigateByUrl('/student');
    } else {
      alert(ret.msg);
    }
  }
}
